package ru.nsu.fit.vinogradov.parser;


public class Lexeme {
    public TerminalSymbol type;
    public String representation;

    public Lexeme(TerminalSymbol type, String representation) {
        this.type = type;
        this.representation = representation;
    }

    public Lexeme(Lexeme other) {
        type = other.type;
        representation = other.representation;
    }

    public boolean isPlusOrMinus() {
        return type == TerminalSymbol.Plus || type == TerminalSymbol.Minus;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Lexeme)) return false;
        return type == ((Lexeme) obj).type &&
                representation.equals(((Lexeme) obj).representation);
    }

    @Override
    public String toString() {
        return "Lexeme{" + type + ", \'" + representation + "\'}";
    }

    public boolean isMulOrDiv() {
        return type == TerminalSymbol.Star || type == TerminalSymbol.Slash;
    }
}
