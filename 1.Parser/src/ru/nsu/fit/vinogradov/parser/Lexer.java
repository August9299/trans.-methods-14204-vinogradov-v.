package ru.nsu.fit.vinogradov.parser;

import ru.nsu.fit.vinogradov.parser.exceptions.BadTerminalSymbolException;

import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;

public class Lexer {
    private Reader reader;
    private boolean symbolWasKept = false;
    private char keptSymbol;

    public Lexer(Reader reader) {
        this.reader = reader;
    }

    public Lexeme getLexeme() throws IOException, BadTerminalSymbolException {
        char c = getNextSymbol(reader);

        NumberResult result = checkForNumber(c, reader);
        if (result.isNumber) {
            if (result.hasNextCharacter) {
                keepNextSymbol(result.nextCharacter);
            }
            return new Lexeme(TerminalSymbol.Number, result.number);
        }

        return chooseNotNumberSymbol(c);
    }

    private Lexeme chooseNotNumberSymbol(char c) throws BadTerminalSymbolException {
        switch (c) {
            case '(':
                return new Lexeme(TerminalSymbol.LeftBracket, Character.toString(c));
            case ')':
                return new Lexeme(TerminalSymbol.RightBracket, Character.toString(c));
            case '+':
                return new Lexeme(TerminalSymbol.Plus, Character.toString(c));
            case '-':
                return new Lexeme(TerminalSymbol.Minus, Character.toString(c));
            case '*':
                return new Lexeme(TerminalSymbol.Star, Character.toString(c));
            case '/':
                return new Lexeme(TerminalSymbol.Slash, Character.toString(c));
            case '^':
                return new Lexeme(TerminalSymbol.Circumflex, Character.toString(c));
            default:
                throw new BadTerminalSymbolException("Bad terminal symbol detected: '" + c + "' (" + (int)c + ")");
        }
    }

    private char getNextSymbol(Reader reader) throws IOException {
        char c;
        if (symbolWasKept) {
            symbolWasKept = false;
            return skipWhitespaces(keptSymbol, reader);
        }
        return skipWhitespaces(reader);
    }

    private char skipWhitespaces(char firstSymbol, Reader reader) throws IOException {
        char c = firstSymbol;
        while (Character.isWhitespace(c)) {
            c = readSymbolWithCheckingEOF(reader);
        }
        return c;
    }

    private void keepNextSymbol(char nextCharacter) {
        keptSymbol = nextCharacter;
        symbolWasKept = true;
    }

    private char skipWhitespaces(Reader reader) throws IOException {
        char c;
        do {
            c = readSymbolWithCheckingEOF(reader);
        } while (Character.isWhitespace(c));
        return c;
    }

    private char readSymbolWithCheckingEOF(Reader reader) throws IOException {
        int readResult = reader.read();
        if (readResult == -1) {
            throw new EOFException();
        }
        char c = (char) readResult;
        return c;
    }

    private NumberResult checkForNumber(char firstSymbol, Reader reader) throws IOException {
        if (!Character.isDigit(firstSymbol)) {
            return new NumberResult().notNumber();
        }

        String number = Character.toString(firstSymbol);
        while (true) {
            try {
                char c = readSymbolWithCheckingEOF(reader);
                if (Character.isDigit(c)) {
                    number += c;
                } else {
                    return new NumberResult(number, c);
                }
            } catch (EOFException e) {
                return new NumberResult(number);
            }
        }
    }

    private class NumberResult {
        public boolean isNumber = false;
        public boolean hasNextCharacter = false;
        public String number;
        public char nextCharacter;

        public NumberResult() {
        }

        public NumberResult(String number) {
            this.isNumber = true;
            this.number = number;
        }

        public NumberResult(String number, char nextCharacter) {
            this.isNumber = true;
            this.number = number;
            this.hasNextCharacter = true;
            this.nextCharacter = nextCharacter;
        }

        public NumberResult notNumber() {
            this.isNumber = false;
            return this;
        }
    }
}
