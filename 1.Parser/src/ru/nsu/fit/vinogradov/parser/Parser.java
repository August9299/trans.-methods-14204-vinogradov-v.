package ru.nsu.fit.vinogradov.parser;

import ru.nsu.fit.vinogradov.parser.exceptions.UnexpectedEOFException;
import ru.nsu.fit.vinogradov.parser.exceptions.UnexpectedLexemeException;
import ru.nsu.fit.vinogradov.parser.exceptions.BadTerminalSymbolException;
import ru.nsu.fit.vinogradov.parser.exceptions.ParserException;

import java.io.EOFException;
import java.io.IOException;

public class Parser {
    private Lexer lexer;
    private Lexeme keptLexeme = null;

    public Parser(Lexer lexer) {
        this.lexer = lexer;
    }

    public void setLexer(Lexer lexer) {
        this.lexer = lexer;
    }

    public double calculate() throws IOException, BadTerminalSymbolException, ParserException {
        keepLexeme(lexer.getLexeme());
        return parseExpression();
    }

    private double parseExpression() throws IOException, BadTerminalSymbolException, UnexpectedLexemeException, UnexpectedEOFException {
        double total = parseTerm();

        boolean expectingTerm = false;
        try {
            Lexeme next = getKeptLexeme();
            while (next.isPlusOrMinus()) {
                Lexeme sign = new Lexeme(next);
                expectingTerm = true;
                keepLexeme(lexer.getLexeme());
                double result = parseTerm();
                total += (sign.type == TerminalSymbol.Plus) ? result : -result;
                expectingTerm = false;
                next = getKeptLexeme();
            }
            keepLexeme(next);
            return total;
        } catch (EOFException e) {
            throw new UnexpectedEOFException("An expected term not found");
        } catch (IllegalStateException e) {
            if (expectingTerm) {
                throw new UnexpectedEOFException("An expected term not found");
            } else {
                return total;
            }
        }
    }

    private double parseTerm() throws UnexpectedEOFException, UnexpectedLexemeException, BadTerminalSymbolException, IOException {
        double total = parseFactor();

        try {
            Lexeme nextLexeme = getKeptLexeme();
            while (nextLexeme.isMulOrDiv()) {
                Lexeme sign = new Lexeme(nextLexeme);
                keepLexeme(lexer.getLexeme());
                double localResult = parseFactor();
                total *= (sign.type == TerminalSymbol.Star) ? localResult : 1.0 / localResult;
                nextLexeme = getKeptLexeme();
            }
            keepLexeme(nextLexeme);
        } catch (IllegalStateException e) {
        } finally {
            return total;
        }
    }

    private double parseFactor() throws IOException, BadTerminalSymbolException, UnexpectedLexemeException, UnexpectedEOFException {
        double power = parsePower();

        try {
            Lexeme nextLexeme = lexer.getLexeme();
            if (nextLexeme.type == TerminalSymbol.Circumflex) {
                keepLexeme(lexer.getLexeme());
                return Math.pow(power, parseFactor());
            } else {
                keepLexeme(nextLexeme);
                return power;
            }
        } catch (EOFException e) {
            return power;
        }

    }

    private double parsePower() throws IOException, BadTerminalSymbolException, UnexpectedLexemeException, UnexpectedEOFException {
        Lexeme lexeme = getKeptLexeme();
        if (lexeme.type == TerminalSymbol.Minus) {
            keepLexeme(lexer.getLexeme());
            return -parseAtom();
        } else {
            keepLexeme(lexeme);
            return parseAtom();
        }
    }

    private double parseAtom() throws IOException, BadTerminalSymbolException, UnexpectedLexemeException, UnexpectedEOFException {
        Lexeme lexeme = getKeptLexeme();
        switch (lexeme.type) {
            case LeftBracket:
                keepLexeme(lexer.getLexeme());
                double result = parseExpression();
                expectRightBracket();
                return result;
            case RightBracket:
                throw new UnexpectedLexemeException("Unexpected right bracket");
            case Number:
                return Integer.parseInt(lexeme.representation);
            default:
                throw new IllegalStateException("Unknown lexeme: " + lexeme);
        }
    }

    private void expectRightBracket() throws IOException, BadTerminalSymbolException, UnexpectedLexemeException, UnexpectedEOFException {
        try {
            Lexeme lexeme = lexemeWasKept() ? getKeptLexeme() : lexer.getLexeme();

            if (lexeme.type != TerminalSymbol.RightBracket) {
                throw new UnexpectedLexemeException("A closing bracket not found");
            }
        } catch (EOFException e) {
            throw new UnexpectedEOFException("A closing bracket not found");
        }
    }

    private boolean lexemeWasKept() {
        return keptLexeme != null;
    }

    private Lexeme getKeptLexeme() {
        if (!lexemeWasKept()) {
            throw new IllegalStateException("There is no lexeme kept");
        }
        Lexeme result = keptLexeme;
        eraseKeptLexeme();
        return result;
    }

    private void keepLexeme(Lexeme lexeme) {
        keptLexeme = lexeme;
    }

    private void eraseKeptLexeme() {
        keptLexeme = null;
    }
}
