package ru.nsu.fit.vinogradov.parser;

public enum TerminalSymbol {
    LeftBracket,
    RightBracket,
    Plus,
    Minus,
    Star,
    Slash,
    Circumflex,
    Number,
}
