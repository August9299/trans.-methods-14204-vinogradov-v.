package ru.nsu.fit.vinogradov.parser.exceptions;


public class BadTerminalSymbolException extends Exception {
    public BadTerminalSymbolException(String message) {
        super(message);
    }
}
