package ru.nsu.fit.vinogradov.parser.tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.nsu.fit.vinogradov.parser.Lexeme;
import ru.nsu.fit.vinogradov.parser.Lexer;
import ru.nsu.fit.vinogradov.parser.TerminalSymbol;
import ru.nsu.fit.vinogradov.parser.exceptions.BadTerminalSymbolException;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;


class LexerTest {
    @Test
    void getLexemeTest() throws IOException, BadTerminalSymbolException {
        Lexer lexer = new Lexer(new StringReader("(   )  +123-98765^*/"));

        Assertions.assertEquals(new Lexeme(TerminalSymbol.LeftBracket, "("), lexer.getLexeme());
        assertEquals(new Lexeme(TerminalSymbol.RightBracket, ")"), lexer.getLexeme());
        assertEquals(new Lexeme(TerminalSymbol.Plus, "+"), lexer.getLexeme());
        assertEquals(new Lexeme(TerminalSymbol.Number, "123"), lexer.getLexeme());
        assertEquals(new Lexeme(TerminalSymbol.Minus, "-"), lexer.getLexeme());
        assertEquals(new Lexeme(TerminalSymbol.Number, "98765"), lexer.getLexeme());
        assertEquals(new Lexeme(TerminalSymbol.Circumflex, "^"), lexer.getLexeme());
        assertEquals(new Lexeme(TerminalSymbol.Star, "*"), lexer.getLexeme());
        assertEquals(new Lexeme(TerminalSymbol.Slash, "/"), lexer.getLexeme());

        assertThrows(EOFException.class, () -> lexer.getLexeme());

        Lexer lexer2 = new Lexer(new StringReader("a"));
        assertThrows(BadTerminalSymbolException.class, () -> lexer2.getLexeme());

        Lexer lexer3 = new Lexer(new StringReader("(111 + 222)"));
        assertEquals(new Lexeme(TerminalSymbol.LeftBracket, "("), lexer3.getLexeme());
        assertEquals(new Lexeme(TerminalSymbol.Number, "111"), lexer3.getLexeme());
        assertEquals(new Lexeme(TerminalSymbol.Plus, "+"), lexer3.getLexeme());
        assertEquals(new Lexeme(TerminalSymbol.Number, "222"), lexer3.getLexeme());
        assertEquals(new Lexeme(TerminalSymbol.RightBracket, ")"), lexer3.getLexeme());
        assertThrows(EOFException.class, () -> lexer3.getLexeme());

    }


}