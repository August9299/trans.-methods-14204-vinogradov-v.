package ru.nsu.fit.vinogradov.parser.tests;

import org.junit.jupiter.api.Test;
import ru.nsu.fit.vinogradov.parser.Lexer;
import ru.nsu.fit.vinogradov.parser.Parser;
import ru.nsu.fit.vinogradov.parser.exceptions.ParserException;
import ru.nsu.fit.vinogradov.parser.exceptions.UnexpectedEOFException;
import ru.nsu.fit.vinogradov.parser.exceptions.UnexpectedLexemeException;
import ru.nsu.fit.vinogradov.parser.exceptions.BadTerminalSymbolException;

import java.io.EOFException;
import java.io.IOException;
import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.*;

class ParserTest {
    @Test
    void atomAndExpression() throws IOException, BadTerminalSymbolException, ParserException {
        Parser parser = new Parser(new Lexer(new StringReader("123")));
        assertEquals(123, parser.calculate());
        assertThrows(EOFException.class, () -> parser.calculate());

        parser.setLexer(new Lexer(new StringReader("(123)")));
        assertEquals(123, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("(111 + 222)")));
        assertEquals(333, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("(1111)+222")));
        assertEquals(1333, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("((111)+222)")));
        assertEquals(333, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("((  111 )  + 222  )")));
        assertEquals(333, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("((((111-222))))")));
        assertEquals(-111, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("(1 + (2)")));
        assertThrows(UnexpectedEOFException.class, () -> parser.calculate());

        parser.setLexer(new Lexer(new StringReader(")")));
        assertThrows(UnexpectedLexemeException.class, () -> parser.calculate());

        parser.setLexer(new Lexer(new StringReader("1 + ")));
        assertThrows(UnexpectedEOFException.class, () -> parser.calculate());
    }

    @Test
    void factorAndPower() throws IOException, BadTerminalSymbolException, ParserException {
        Parser parser = new Parser(new Lexer(new StringReader("123")));
        assertEquals(123, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("-123")));
        assertEquals(-123, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("-(123)")));
        assertEquals(-123, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("-(123+123)")));
        assertEquals(-246, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("123")));
        assertEquals(123, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("-123")));
        assertEquals(-123, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("2^3")));
        assertEquals(8, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("2^-3")));
        assertEquals(1.0/8, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("2^3^2")));
        assertEquals(512, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("(-2)^4")));
        assertEquals(16, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("(-2)^(-3)")));
        assertEquals(Math.pow(-2, -3), parser.calculate());

        parser.setLexer(new Lexer(new StringReader("2^(-3)^(-3)")));
        assertEquals(Math.pow(2, Math.pow(-3, -3)), parser.calculate());

        parser.setLexer(new Lexer(new StringReader("512-2^3^(4-2)")));
        assertEquals(512-Math.pow(2, Math.pow(3, 2)), parser.calculate());
    }

    @Test
    void term() throws ParserException, IOException, BadTerminalSymbolException {
        Parser parser = new Parser(new Lexer(new StringReader("2*3")));
        assertEquals(6, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("2*3*4")));
        assertEquals(24, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("2*3*4 - 2*12")));
        assertEquals(0, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("2*3*(3+1)")));
        assertEquals(24, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("(2*3*4)")));
        assertEquals(24, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("16 - 2^3*2")));
        assertEquals(0, parser.calculate());

        parser.setLexer(new Lexer(new StringReader("16 - 2^(6 / 3)^2")));
        assertEquals(0, parser.calculate());
    }
}